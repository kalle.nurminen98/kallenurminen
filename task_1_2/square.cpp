// Specifications
#include <iostream>
using namespace std;
 
int main()
{
    // Defining a variable for rows and columns
    int rows_and_columns;
    // User input for rows and columns
    cout << "Enter number of rows and columns: ";
    cin >> rows_and_columns;
    // Putting valid integer to false to get in while loop
    bool valid_integer = false;
    while (!valid_integer)
    {
        // Checking if the input is correct
        if (cin.fail() or rows_and_columns < 0)
        {
            // This corrects the input
            cin.clear();
            // This skips the left over input data
            cin.ignore();
            // Error message
            cout << "Error, enter positive integer only ";
            cout << endl;
            // User input for rows and columns
            cout << "Enter number of rows and columns: ";
            cin >> rows_and_columns;
            // Input was not an positive integer so user tries again
            valid_integer = false;
        }

        // Changing it to true because if its correct input
        else
        {
            valid_integer = true;
        }
        
    }
    // Defining variables for for loops
    int i = 0;
    int j = 0;
    // First for loop
    for (i = 1; i <= rows_and_columns; i++)
    {
        // Second for loop
        for (j = 1; j <= rows_and_columns; j++)
        {
            // Prints star if statement is correct, otherwise it prints empty space
            // Statement is correct if at least two of the statements are correct
            if (i == 1 || i == rows_and_columns || j == 1 || j ==rows_and_columns)
            {
                cout << "* ";
            }
            else
            {
                cout << "  ";
            }
        }
        // Next row
        cout << endl;
    }
    // Ending text
    std::cout << "Press any key to continue. . .\n";
    return 0;
}
 